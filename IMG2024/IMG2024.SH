    ##  SC[ hkmf-mini] #########################################
    ## SEE[ hkmf-c11 ] for reference:  #########################
    ############################################################
    gcc                                                        \
        -x c                                                   \
        -c "IMG2024.C11"                                       \
        -o object_file.o                                       \
                                                               \
            -Werror                                            \
            -Wfatal-errors                                     \
            -Wpedantic                                         \
            -Wall                                              \
            -Wextra                                            \
                                                               \
            -fstrict-aliasing                                  \
            -Wstrict-aliasing                                  \
                                                               \
            -D FOREVER_ALONE_COMPILE                           \
                                                               \
            -std=c11                                           \
            -m64 ###############################################
                                        ####                ####
    gcc -o IMG2024.exe object_file.o    ####                ####                
    rm                 object_file.o    ####                ####     
         ./IMG2024.exe                  ####                ####
    rm     IMG2024.exe                  ####                ####
                                        ####                ####
    read -p "[ENTER_TO_EXIT]:"          ####                ####
    ############################################################