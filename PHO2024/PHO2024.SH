    ##  SC[ hkmf-mini] #########################################
    ## SEE[ hkmf-c11 ] for reference:  #########################
    ############################################################
    gcc                                                        \
        -x c                                                   \
        -c "PHO2024.C11"                                       \
        -o object_file.o                                       \
                                                               \
            -Werror                                            \
            -Wfatal-errors                                     \
            -Wpedantic                                         \
            -Wall                                              \
            -Wextra                                            \
                                                               \
            -fstrict-aliasing                                  \
            -Wstrict-aliasing                                  \
                                                               \
            -D COMPILE_EXE_PHO2024                             \
                                                               \
            -std=c11                                           \
            -m64 ###############################################
                                        ####                ####
    gcc -o PHO2024.exe object_file.o    ####                ####                
    rm                 object_file.o    ####                ####     
         ./PHO2024.exe                  ####                ####
    rm     PHO2024.exe                  ####                ####
                                        ####                ####
    read -p "[ENTER_TO_EXIT]:"          ####                ####
    ############################################################