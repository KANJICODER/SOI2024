
![Alt text](./ADVERT.PNG)

# SOI2024 Library

SOI2024 is used to :

1. Bit Pack Pre-Scaled RGB(NO A) Images Into An RGBA .PNG file
2. Serve as a zero-cost abstraction obfuscated asset pack

## Usage

To use SOI2024 in your project, follow these steps:

```bash
cd SOI2024
./ALT.SH
```

Or two lines in C code to compile demo :

```c
#define COMPILE_ALT_SOI2024
#include "./SOI2024.C11"
```

Or if you want to get a bit more involved :

```c
#include "./SOI2024.C11"

int main(void) {

    F_SOI2024_INI();;;;;; //: Initialize The System  ::::::: ://
    F_SOI2024_UTM();;;;;; //: Run The Tests          ::::::: ://

    d_soi2024_pretend_iam_not_kanjicoder =( 1 );

    /** Create custom soiform for this demo **/
    SOIFORM stk_soiform={
    
        .m_relfold =( "./IGNORE/DEMO" )
    ,   .m_prefix3 =(          "DEM"  )  
    
    };

    /** Create the 80 512_X_512 png files our **/
    /** demo code will use as inputs          **/
    F_SOI2024_PLACEHO_MAKEPNG_USE_SOIFORM(
                            &( stk_soiform ) );
    
    /** Process the placeholder images we just made    **/
    /** into an obfuscated[ 512_X_8192 ]asset pack.    **/
    F_SOI2024_PROCESS_SOIFORM( &( stk_soiform ) );

    /** ********************************************** **/
    /** Extract the asset pack currently in ram to the **/
    /** file system to prove that we actually encoded  **/
    /** RGB ( NO ALPHA) images inside a [ 512_X_8192 ] **/
    /** RGBA image .                                   **/
    /** ********************************************** **/
    F_SOI2024_EXTRACT(  );  

    return( 0 );

}
```

## Technical Details

   Remember , all memory is 1-dimensional , load the
   giant RGBA[ 512_X_8192 ]image into memory , grab the correct
   addresses , and now you've got your original RGB( NO A)images !