    ##  SC[ hkmf-mini] #########################################
    ## SEE[ hkmf-c11 ] for reference:  #########################
    ############################################################
    gcc                                                        \
        -x c                                                   \
        -c "SOI2024.C11"                                       \
        -o object_file.o                                       \
                                                               \
            -Werror                                            \
            -Wfatal-errors                                     \
            -Wpedantic                                         \
            -Wall                                              \
            -Wextra                                            \
                                                               \
            -fstrict-aliasing                                  \
            -Wstrict-aliasing                                  \
                                                               \
            -D COMPILE_EXE_SOI2024                             \
                                                               \
            -std=c11                                           \
            -m64 ###############################################
                                        ####                ####
    gcc -o SOI2024.exe object_file.o    ####                ####                
    rm                 object_file.o    ####                ####     
         ./SOI2024.exe                  ####                ####
    rm     SOI2024.exe                  ####                ####
                                        ####                ####
    read -p "[ENTER_TO_EXIT]:"          ####                ####
    ############################################################